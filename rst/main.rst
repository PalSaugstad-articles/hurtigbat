.. raw:: html

   <!-- (php # 2 __DATE__ Hurtigbåter) -->
   <!-- ($attrib_AC=0;) -->

========================
Utslippsfrie hurtigbåter
========================

*Av: |author| / |datetime|*

.. include:: revision-header.rst

Rapporten
`"Utslippsfri båtrute i Oslofjorden - Forprosjekt"
<http://www.bfk.no/Documents/BFK/Regionalutvikling/Rapport-%20forprosjekt%20utslippsfri%20b%C3%A5trute%20i%20Oslofjorden.pdf>`_
fra
Buskerud fylkeskommune,
Akershus fylkeskommune
og Oslo kommune
datert 2017-08-11 er interessant lesning.
Det er en grundig rapport som sammenlikner forskjellige alternativer for hurtigbåt-trafikk på Oslofjorden.
Det er de to eksisterende hurtigbåtforbindelsene som er vurdert, og nytteverdien av å evt. erstatte de nåværende
hurtigbåtene som har diesel-drift med båter som er basert på elektrisk energi.

Rapporten er ment som et grunnlagsdokument for at man skal kunne ta politiske avgjørelser på et kunnskapsbasert grunnlag.
Jeg ser endel problemer med dokumentet.

Flere alternativer burde ha blitt kvantifisert
----------------------------------------------

Ruten Slemmestad - Vollen - Aker Brygge
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

I tekstlig form diskuterer rapporten alternative ruteopplegg på land (buss/tog).
Endel betrakninger om nødvendig utbygging av infrastruktur (jernbane/kollektivfelt) er nevnt, og tidsforbruk langs sjøen kontra på land er belyst.
Konklusjonen er at reisetiden er sammenliknbar, dog noe lenger langs land i rushene
grunnet at det ikke er gjennomgående kollektivfelt langs hele traséen.

Det som mangler er en sammenlikning av energiforbruket og forurensning ved en land-basert løsning.
Rapporten åpner med at energiforbruket for hurtigbåt er svært høyt.
Andre rapporter stipulerer energiforbruket for hurtigbåter til å være omlag 6 ganger så høyt som for buss pr. person-km.
CO\ :sub:`2`-utslippet blir tilsvarende lavere for buss.
Rapporten angir at CO\ :sub:`2`-utslippet fra en hydrogen-drevet hurtigbåt blir omlag 1/3 av utslippet ved diesel-drevet hurtigbåt.
Hvis vi altså sammenlikner CO\ :sub:`2`-utslippet for den (i rapporten) foretrukne
hydrogen-hurtigbåten med helt ordinær dieselbuss, så har altså **hydrogen-hurtigbåten omlag dobbelt så høyt CO\ :sub:`2`-utslipp**!
Sammenliknet med *elektrisk* buss blir hydrogen-hurtigbåten enda verre.

Rapporten diskuterer at det kan være ønskelig å øke bruken av denne hurtigbåt-forbindelsen.
Hvorfor det, tro?
I og med at hurtigbåten er miljø-taperen, vil flytting av passasjerer fra bil og buss til hurtigbåt slå ut i økt energibehov og forurensning.
Å tilpasse driften slik at belegget blir vesentlig høyere enn nå, slik som rapporten antyder, er vanskelig å få til i praksis.
Så, når både busser, biler og hurtigbåter en gang i framtiden blir elektriske, så kommer hurtigbåten fortsatt til å havne på en soleklar jumbo-plass,
langt under de veibaserte alternativene.

Trafikkvolumet, ca. 345 daglige pendlere i løpet av 90 minutter, er for lite til at det nytter å argumentere med at hurtigbåt-trafikken
fører til en vesentlig avlastning på veinettet på denne strekningen.
Hvis alle kjører hver sin privatbil utgjør dette riktignok omtrent én ekstra bil hvert 15. sekund, hvilket ikke er neglisjerbart.
En god del vil nok velge buss istedetfor bil, og da utgjør ikke den økte veibelastningen en vesentlig faktor.

Ruten Nesodden - Lysaker
^^^^^^^^^^^^^^^^^^^^^^^^

Her er omveien langs land såpass lang at det ikke er aktuelt å gjøre en sammenlikning med buss.
Derimot er konvensjonell båt et bra alternativ.
Overfartstiden blir lenger med konvensjonell båt, f. eks. 16 minutter sammenliknet med 8 minutter.
Energibesparelsen ved konvensjonell fart blir ganske stor.
På sjøen er det en tommelfingerregel som sier at energiforbruket pr. tidsenhet øker med 3. potens av hastigheten.
Mao., hvis man halverer hastigheten, trenger man 1/8 så stor motorkraft.
Siden overfarten da tar dobbelt så lang tid, vil energiforbruket synke til 1/4.

I rapporten blir hydrogendrift foretrukket særlig fordi man kan fylle hydrogen kun én gang pr. dag, ikke ved hvert anløp.
For en konvensjonell båt er batteridrift mer aktuelt i og med at ladetiden ved hvert anløp blir betraktelig redusert.
Dessuten er det viktig å spare mest mulig vekt for en hurtigbåt.
For en konvensjonell båt kan man ha med seg litt ekstra batterikapasitet uten at man trenger å bekymre seg for vektøkningen.
Rapporten viser også at energiforbruket blir langt lavere ved batteridrift enn ved hydrogendrift.
Dette skyldes at tapene ved å konvertere strøm til hydrogen og tilbake til strøm igjen er betydelig større enn
tapene ved batteri-drift.
Siden båter i utgangpunktet har langt høyere forbruk enn landbasert transport, bør etter min mening
lavt forbruk vektlegges høyt.
En konvensjonell batteridrevet båt er trolig det beste valget for denne strekningen.

Er (utslippsfrie) hurtigbåter framtidsrettet?
---------------------------------------------

Rapporten diskuterer eksport-potensialet for denne teknologien.
Jeg tror at hurtigbåt aldri vil kunne bli et transportmiddel egnet for massetransport. Det vil forbli som det er idag,
et nisjeprodukt som står for 1 promille eller så av transport-volumet.
Man bør heller legge arbeid i å elektrifisere bussdriften, samt intensivere satsningen på batteridrevne konvensjonelle fartøyer.
Både kostnadsnivå, miljømessige betraktninger, teknologisk tilgjengelighet og anvendlighet taler for dette.

.. raw:: html

   <hr>
   <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Utslippsfrie hurtigbåter">Tweet</a>
   <br>
   <a target="_blank" href="show?f=articles/parsed/hurtigbat/nor/hurtigbat.pdf">som pdf</a>
